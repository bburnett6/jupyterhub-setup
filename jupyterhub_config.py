
#AUTHENTICATION
#use default pam. For more see: https://github.com/jupyterhub/jupyterhub/wiki/Authenticators

#SSL
#c.JupyterHub.ssl_cert = '/etc/ssl/certs/carnie.crt'
#c.JupyterHub.ssl_key = '/etc/ssl/certs/carnie.key'
c.JupyterHub.ssl_cert = '/sw/jupyterhub/jup-cert.crt'
c.JupyterHub.ssl_key = '/sw/jupyterhub/jup-cert.key'

#Internal SSL (secures independent user notebook traffic)
#See https://jupyterhub.readthedocs.io/en/stable/reference/spawners.html#encryption
#NOTE THIS DOES NOT WORK WITH THE SLURM SPAWNER. Open issue: https://github.com/jupyterhub/batchspawner/issues/192
#This almost works. On an nfs, all that is needed is to transfer ownership of the root created cert 
#{internal_certs_location}/user-{username} to the user before the job runs. 
#For some reason, I could not get the move_certs function to run either. Tried hacking the exec_prefix, no good
"""
Tried to do this for move_certs but I never see the log output. I think it doesn't work because c.JupyterHub is a config and you cant override a function within a config. There has to be another way to override
see https://discourse.jupyter.org/t/sudospawner-and-internal-ssl/2477/3
async def my_move_certs(self, paths):
	import os, shutil
        self.log.info('########## Im doin my thing #############')
        user_ssl_path = os.path.join(c.Jupyterhub.internal_certs_location, f'user-{self.user.name}')
        for dirpath, dirnames, filenames in os.walk(user_ssl_path):
        	shutil.chown(dirpath, self.user.name)
                for filename in filenames:
                	shutil.chown(os.path.join(dirpath, filename), self.user.name)
        #no modifications to the path to the actual certs were changed, so just return the input
	return paths

c.JupyterHub.Spawner.move_certs = my_move_certs
"""
#c.JupyterHub.internal_ssl = True
#c.JupyterHub.internal_certs_location = '/sw/jupyterhub/internal-ssl' #if changes are made, remove this dir
#c.JupyterHub.trusted_alt_names = ['DNS:carnie.cscvr.umassd.edu']
#c.Spawner.ssl_alt_names = ['DNS:carnie.cscvr.umassd.edu'] + [f'DNS:node{x}' for x in range(50)] #add all nodes to ssl altnames
#c.SlurmSpawner.exec_prefix = 'chown -R {username} /sw/jupyterhub/internal-ssl/user-{username}; sudo -E -u {username}'

#Testing
c.JupyterHub.bind_url = 'http://:8686/'
c.JupyterHub.hub_ip = '0.0.0.0'
c.JupyterHub.hub_port = 8678
#c.JupyterHub.hub_connect_ip = 'master'

#Spawner config
import batchspawner
c.JupyterHub.spawner_class = 'wrapspawner.ProfilesSpawner'
c.Spawner.http_timeout = 20 #time to wait for spawners to start (s) if slurm queue is full jupyter will cancel job
c.Spawner.default_url = '/lab'
c.Spawner.cmd = '/sw/jupyterhub/run_cmd.py'
c.SlurmSpawner.batch_script = """#!/bin/bash -l
#SBATCH --output={{homedir}}/jupyterhub_slurmspawner_%j.log
#SBATCH --job-name=spawner-jupyterhub
#SBATCH --chdir={{homedir}}
#SBATCH --export=ALL
#SBATCH --get-user-env=L
{% if partition  %}#SBATCH --partition={{partition}}
{% endif %}{% if runtime    %}#SBATCH --time={{runtime}}
{% endif %}{% if memory     %}#SBATCH --mem={{memory}}
{% endif %}{% if gres       %}#SBATCH --gres={{gres}}
{% endif %}{% if nprocs     %}#SBATCH --cpus-per-task={{nprocs}}
{% endif %}{% if reservation%}#SBATCH --reservation={{reservation}}
{% endif %}{% if options    %}#SBATCH {{options}}{% endif %}
set -euo pipefail
trap 'echo SIGTERM received' TERM
{{prologue}}
module load jupyterhub
which jupyterhub-singleuser
{% if srun %}{{srun}} {% endif %}{{cmd}}
echo "jupyterhub-singleuser ended gracefully"
{{epilogue}}
"""

c.ProfilesSpawner.profiles = [
	("Home Browser (Local: No computation please)", 'local', 'jupyterhub.spawner.LocalProcessSpawner', {'ip':'0.0.0.0'} ),
	("Small: 4 Core, 8 GB, 8 hours, M2050", 'sm4c8gb8h', 'batchspawner.SlurmSpawner', 
	dict(partition='short-single', runtime='8:00:00', memory='8000', gres='gpu:M2050:1', nprocs='4')),
	("Medium: 12 Core, 16 GB, 8 hours, M2050", 'md12c16gb8h', 'batchspawner.SlurmSpawner', 
	dict(partition='short-single', runtime='8:00:00', memory='16000', gres='gpu:M2050:1', nprocs='12')),
	("Large: 24 Core, 40 GB, 8 hours, M2050", 'lg48c40gb8h', 'batchspawner.SlurmSpawner',
	dict(partition='short-single', runtime='8:00:00', memory='40000', gres='gpu:M2050:1', nprocs='24')),
	("Long: 24 Core, 40 GB, max 28 Day, M2050", 'lg48c40gb28d', 'batchspawner.SlurmSpawner',
	dict(partition='long-single', runtime='28-00:00:00', memory='40000', gres='gpu:M2050:1', nprocs='24')),
	("V100: 24 Core, 40 GB, 2 Day, V100", 'lg48c40gb8h', 'batchspawner.SlurmSpawner',
	dict(partition='long-single', runtime='2-00:00:00', memory='40000', gres='gpu:V100:1', nprocs='24')),
]
#c.ProfilesSpawner.ip = '0.0.0.0'
