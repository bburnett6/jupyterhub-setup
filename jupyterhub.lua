help([[
Load Anaconda for Jupyterhub
]])

prepend_path("PATH", "/sw/jupyterhub/anaconda/bin")

execute {cmd="source /sw/jupyterhub/anaconda3/bin/activate base", modeA={"load"}}

execute {cmd="conda deactivate", modeA={"unload"}}
execute {cmd="conda deactivate", modeA={"unload"}}
