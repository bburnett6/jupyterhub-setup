#!/bin/bash

source /usr/share/lmod/lmod/init/bash
module load jupyterhub

nohup jupyterhub -f jupyterhub_batchspawner_config.py &> jupyterhub.log &
