#!/sw/jupyterhub/anaconda3/bin/python

"""
This script needs to be in python because the jupyterhub script that calls this
exec's this script. Exec can only interpret python so... yeah.
"""

import os
import sys
from shutil import which
from runpy import run_path

"""
Taken from the module init python script. That script only works for
python2. This function is just a modification to work with python3
"""
def module(command, *arguments):
  commands = os.popen('/usr/share/lmod/lmod/libexec/lmod python %s %s'\
                      % (command, " ".join(arguments))).read()
  exec(commands)

def main():
	#init modules
	os.environ['MODULEPATH'] = '/mcms/modulefiles/Core'
	module("load", "jupyterhub")

	cmd_path = which("jupyterhub-singleuser")
	#sys.argv = sys.argv[1:]
	run_path(cmd_path, run_name='__main__')

	
if __name__ == "__main__":
	main()
