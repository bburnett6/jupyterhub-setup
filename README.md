# Installation

I was working in our modules directory `/sw`. In there I create a directory `jupyterhub` for the module so the base working path is `/sw/jupyterhub`. 

From here I placed a fresh install of anaconda3 (ie download the installer script and run it with prefix `/sw/jupyterhub/anaconda3`. I then use the module file `jupyterhub.lua` to work inside this module.

The packages I needed to install (after the `module load jupyterhub`) were:

`pip install wrapperspawner batchspawner`

You can then edit the `jupyterhub_config.py` for the site specific details.

Start with `jupyterhub -f jupyterhub_config.py` or use a similar `start_jupyterhub.sh` script and turn it into a daemon 

# Notes

* Internal SSL is not working. See the notes inside the config file for more details
* The `run_cmd.py` file is necessary for working in a module framework and it must be a python script. See file for more notes
* Users can create their own kernels for package installation with: https://ipython.readthedocs.io/en/stable/install/kernel_install.html#kernels-for-different-environments
* Don't forget to turn on dark mode :) 
